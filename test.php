<?php

$curl= curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$result= curl_exec($curl);
curl_close($curl);

$result= json_decode($result, true);
// print_r($result);die;

$global= $result['Global'];
// print_r($global);die;
$countries= $result['Countries'];
// print_r($countries);die;

$temp_arr=array();
foreach($countries as $key=>$value){
    $temp_arr[]=$value['Country'];
}

// total convirmed
$temp_convirmed=array();
foreach($countries as $key=>$value){
    $temp_convirmed[]=$value['TotalConfirmed'];
}

// total detahs
$temp_deaths=array();
foreach($countries as $key=>$value){
    $temp_deaths[]=$value['TotalDeaths'];
}

// total detahs
$temp_recovered=array();
foreach($countries as $key=>$value){
    $temp_recovered[]=$value['TotalRecovered'];
}

$temp_color=array();

$dinamic=function (){
    $r= rand(0,255);
    $g= rand(0,255);
    $b= rand(0,255);
    return "rgb(".$r.",".$g.",".$b.")";
};

foreach($countries as $x=>$y){
    // print_r(count($y));die;
    for($i=1; $i<=count($y); $i++){
        $temp_color[]=$dinamic();
    }
    // print_r($temp_color);die;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>curl</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
      
    <div class="container">
        <h1 class="text-center pt-3 border-bottom">Data Covid-19</h1>
        <div class="row d-flex justify-content-center p-5">

            <div class="col-12 p-2 pb-3">
                <h2 class="text-center">Global</h2>
                <div class="card" style="width: 18rem;margin-left:36%;">
                    <div class="card-header">
                        Data Global
                    </div>
                    <ul class="list-group list-group-flush">
                        <?php foreach($global as $key=>$Value) : ?>
                        <li class="list-group-item"><?= $key; ?> : <?= $global[$key]; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>

            <div class="col-12 c">
                <canvas id="state"></canvas>
            </div>
            
            <div class="col-6 mt-5">
                <h2 class="text-center">Total Confirmed</h2>
                <canvas id="myChart"></canvas>
            </div>

            <div class="col-6 my-5">
                <h2 class="text-center">Total Deaths</h2>
                <canvas id="deaths"></canvas>
            </div>
                
            <div class="col-10">
                <h2 class="text-center">Total Recovered</h2>
                <canvas id="recovered"></canvas>
            </div>

        </div>
    </div>

    <!-- footer -->
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center border-top mb-1" id="last_update">

            </div>
        </div>
    </div>

    <script>

        var ctx = document.getElementById('state').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {

                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
        });

    
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {

                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    data: <?= json_encode($temp_convirmed); ?>,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
            options:{
                legend:{
                    display:false
                }
            }
        });

        var ctx = document.getElementById('deaths').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {

                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    data: <?= json_encode($temp_deaths); ?>,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
            options:{
                legend:{
                    display:false
                }
            }
        });

        var ctx = document.getElementById('recovered').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {

                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: '# of Votes',
                    data: <?= json_encode($temp_deaths); ?>,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
            options:{
                legend:{
                    display:false
                }
            }
        });

        $("#last_update").html('Last Update: '+(new Date));
    
    </script>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</body>
</html>