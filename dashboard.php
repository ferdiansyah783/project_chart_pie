<?php

$curl=curl_init();
curl_setopt($curl, CURLOPT_URL, 'https://api.covid19api.com/summary');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$output= curl_exec($curl);
curl_close($curl);

$output=json_decode($output, true);

$global=$output['Global'];

$nc= $output['Global']['NewConfirmed'];
$c= $output['Global']['TotalConfirmed'];
$nd= $output['Global']['NewDeaths'];
$d= $output['Global']['TotalDeaths'];
$nr= $output['Global']['NewRecovered'];    
$r= $output['Global']['TotalRecovered'];    

$countries=$output['Countries'];

$temp_arr=array();
foreach($countries as $key=>$value){
    $temp_arr[]=$value['Country'];
}

// new confirmed
$temp_newConfirm=array();
foreach($countries as $key=>$value){
    $temp_newConfirm[]=$value['NewConfirmed'];
}
// new deaths
$temp_newDeaths=array();
foreach($countries as $key=>$value){
    $temp_newDeaths[]=$value['NewDeaths'];
}
// new recovered
$temp_newRecover=array();
foreach($countries as $key=>$value){
    $temp_newRecover[]=$value['NewRecovered'];
}

// total convirmed
$temp_convirmed=array();
foreach($countries as $key=>$value){
    $temp_convirmed[]=$value['TotalConfirmed'];
}

// total detahs
$temp_deaths=array();
foreach($countries as $key=>$value){
    $temp_deaths[]=$value['TotalDeaths'];
}

// total detahs
$temp_recovered=array();
foreach($countries as $key=>$value){
    $temp_recovered[]=$value['TotalRecovered'];
}

$temp_color=array();

$dinamic=function (){
    $r= rand(0,255);
    $g= rand(0,255);
    $b= rand(0,255);
    return "rgb(".$r.",".$g.",".$b.")";
};

foreach($countries as $x=>$y){
    // print_r(count($y));die;
    for($i=1; $i<=count($y); $i++){
        $temp_color[]=$dinamic();
    }
    // print_r($temp_color);die;
}



?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="icon" href="assets/icon.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="assets/style.css">

    <title>Dashboard!</title>
    <style>
        
    </style>
</head>
<body>
    
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 col-sm-10 col-md-8 col-lg-10">
                <p class="" style="font-size: 2rem;">Dashboard of the COVID-19 Virus  
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" onclick="myFunction()" id="customSwitch1">
                        <label class="custom-control-label" for="customSwitch1">Trun on the light mode</label>
                    </div>
                </p>
            </div>

            <div class="col-7 col-sm-2 col-md-4 col-lg-2 border rounded" style="height: 2.3rem;">
                <svg width="1.5rem" height="1.5rem" viewBox="0 0 16 16" class="bi bi-calendar-check" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                <path fill-rule="evenodd" d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                </svg>
                <span class="last">Last Update:<?php echo date('d  M  y'); ?></span>
            </div>

        </div>
    </div>

    <div class="container-fluid my-3">
        <div class="row d-flex justify-content-center" id="newFont">

            <div class="border rounded" id="new">
                <p class="pl-3 ">New Confirmed</p>
                <canvas id="newMyChart"></canvas>
            </div>

            <div class="border rounded mx-2" id="new">
                <p class="pl-3 ">New Deaths</p>
                <canvas id="newDeaths"></canvas>
            </div>

            <div class="border rounded" id="new">
                <p class="pl-3 ">New Recovered</p>
                <canvas id="newRecovered"></canvas>
            </div>

        </div>
    </div>

    <div class="container-fluid my-3">
        <div class="row d-flex justify-content-center">

            <div class="mr-2 border rounded" id="total">
                <div class="border-bottom">
                    <p class="pl-3 m-0 "><b>Number of casses</b></p>
                    <span class="pl-3" style="font-size:1em;"><?php echo date('d  M  y'); ?></span>
                </div>
                <div>
                    <p style="text-align: center;">
                        <img class="img" src="assets/background.png" width="600" alt="">
                    </p>
                </div>
                <div class="mt-2">
                    <p class="total p-3 ">Total Confirmed</p>
                    <canvas id="barConfirmed"></canvas>
                </div>
                <div class="mt-2">
                    <p class="total p-3 ">Total Deaths</p>
                    <canvas id="barDeaths"></canvas>
                </div>
                <div class="mt-2">
                    <p class="total p-3 ">Total Recovered</p>
                    <canvas id="barRecovered"></canvas>
                </div>
            </div>
                
            <div class="global border rounded">
                <div class="border-bottom pb-2">
                    <p class="pl-3 m-0 text-center "><b>Global</b></p>
                </div>

                <div class="my-2" id="newGlobal">
                    <p class="text-center text-primary"><?= number_format($nc); ?></p>
                    <p class="fontNew text-center ">New Confirmed</p>
                    <p class="text-center text-primary"><?= number_format($c); ?></p>
                    <p class="fontNew text-center ">Total Confirmed</p>
                    <p class="text-center text-danger"><?= number_format($nd); ?></p>
                    <p class="fontNew text-center">New Deaths</p>
                    <p class="text-center text-danger"><?= number_format($d); ?></p>
                    <p class="fontNew text-center">Total Deaths</p>
                    <p class="text-center text-warning"><?= number_format($nr); ?></p>
                    <p class="fontNew text-center">New Recovered</p>
                    <p class="text-center text-warning"><?= number_format($r); ?></p>
                    <p class="fontNew text-center">Total Recovered</p>
                </div>

                <div class="mt-4 border-bottom">
                    <p style="font-size: 1.5rem;" class="pl-3 m-0 text-center "><b>Gender</b></p>
                </div>
                <div class="mt-3">
                    <canvas id="gender"></canvas>
                </div>
                <div class="mt-5" id="deffinition">
                    <p class="p-1 m-0" id="fontDeff"><b>1. Definition Corona Virus and COVID-19</b></p>
                    <p class="p-2 m-0 ">Coronaviruses are a large family of viruses that cause disease in humans and animals. In humans usually
                        causes respiratory tract infections, from the common cold to serious illnesses like the Middle East
                        Respiratory Syndrome (MERS) and Severe Acute Respiratory Syndrome (SARS). Coronavirus
                        new strains discovered in humans since the outbreak emerged in Wuhan China, in December 2019, later
                        given the name Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-COV2), and causes Coronavirus Disease-2019 (COVID-19)
                    </p>
                    <div class="fontDeffDis">
                        <p class="p-1 mt-3 m-0" id="fontDeff"><b>2. what are the symptoms of covid-19</b></p>
                        <p class="p-2 m-0 ">Common symptoms include fever ≥380C, dry cough, and shortness of breath. If there are people who in the 14 days before symptoms appear
                            have traveled to an infected country, or have cared for / had close contact with a person with COVID-19
                            the person will be subjected to further laboratory tests to confirm the diagnosis. List of affected countries can
                            monitored through
                        </p>
                        <p class="p-1 mt-3 m-0" id="fontDeff"><b>3. Can the virus that causes COVID-19 be transmitted through the air?</b></p>
                        <p class="p-2 m-0 ">Not. Until now, research states that the virus that causes COVID-19 is transmitted through contact with small droplets 
                            from the respiratory tract.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-12 border-top mt-5 p-1">
                <p class="float-left"> 
                    <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-shield-plus m-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M5.443 1.991a60.17 60.17 0 0 0-2.725.802.454.454 0 0 0-.315.366C1.87 7.056 3.1 9.9 4.567 11.773c.736.94 1.533 1.636 2.197 2.093.333.228.626.394.857.5.116.053.21.089.282.11A.73.73 0 0 0 8 14.5c.007-.001.038-.005.097-.023.072-.022.166-.058.282-.111.23-.106.525-.272.857-.5a10.197 10.197 0 0 0 2.197-2.093C12.9 9.9 14.13 7.056 13.597 3.159a.454.454 0 0 0-.315-.366c-.626-.2-1.682-.526-2.725-.802C9.491 1.71 8.51 1.5 8 1.5c-.51 0-1.49.21-2.557.491zm-.256-.966C6.23.749 7.337.5 8 .5c.662 0 1.77.249 2.813.525a61.09 61.09 0 0 1 2.772.815c.528.168.926.623 1.003 1.184.573 4.197-.756 7.307-2.367 9.365a11.191 11.191 0 0 1-2.418 2.3 6.942 6.942 0 0 1-1.007.586c-.27.124-.558.225-.796.225s-.526-.101-.796-.225a6.908 6.908 0 0 1-1.007-.586 11.192 11.192 0 0 1-2.417-2.3C2.167 10.331.839 7.221 1.412 3.024A1.454 1.454 0 0 1 2.415 1.84a61.11 61.11 0 0 1 2.772-.815z"/>
                        <path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 .5-.5z"/>
                    </svg>
                    <p class="foot">Jepara Health Emergency Dashboard  Jeapara (COVID-19) Hompage</p>
                </p>
            </div>

        </div>
    </div>
    

    <script>
        var ctx = document.getElementById('newMyChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: 'new confirmed',
                    data: <?= json_encode($temp_newConfirm); ?>,
                    fill: false,
                    borderColor: [
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var ctx = document.getElementById('newDeaths').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: 'new deaths',
                    data: <?= json_encode($temp_newDeaths); ?>,
                    fill: false,
                    borderColor: [
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var ctx = document.getElementById('newRecovered').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: 'new recovered',
                    data: <?= json_encode($temp_newRecover); ?>,
                    fill: false,
                    borderColor: [
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var ctx = document.getElementById('barConfirmed').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: 'total confirmed',
                    data: <?= json_encode($temp_convirmed); ?>,
                    // fill: false,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var ctx = document.getElementById('barDeaths').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: 'total deaths',
                    data: <?= json_encode($temp_deaths); ?>,
                    // fill: false,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var ctx = document.getElementById('barRecovered').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($temp_arr); ?>,
                datasets: [{
                    label: 'total recovered',
                    data: <?= json_encode($temp_recovered); ?>,
                    // fill: false,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });

        var ctx = document.getElementById('gender').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['58% male','38% female'],
                datasets: [{
                    label: '%',
                    data: [58,38],
                    // fill: false,
                    backgroundColor: <?= json_encode($temp_color); ?>,
                    borderWidth: 1
                }]
            },
        });

        function myFunction() {
            var element = document.body;
            element.classList.toggle("light-mode");
        }
    </script>

      

   
    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
   
  </body>
</html>